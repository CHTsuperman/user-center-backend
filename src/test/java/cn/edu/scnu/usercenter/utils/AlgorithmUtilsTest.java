package cn.edu.scnu.usercenter.utils;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author 崔恒拓
 * @version 1.0
 */
@SpringBootTest
class AlgorithmUtilsTest {

    @Test
    void test(){
        List<String> tagList1 = Arrays.asList("Java", "研一", "男");
        List<String> tagList2 = Arrays.asList("Java", "研二", "男");
        List<String> tagList3 = Arrays.asList("C++", "研一", "女");
        long score1 = AlgorithmUtils.minDistance(tagList1, tagList2);
        long score2 = AlgorithmUtils.minDistance(tagList1, tagList3);
        long score3 = AlgorithmUtils.minDistance(tagList2, tagList3);
        System.out.println(score1);
        System.out.println(score2);
        System.out.println(score3);
    }

}