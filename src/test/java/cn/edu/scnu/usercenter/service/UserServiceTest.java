package cn.edu.scnu.usercenter.service;

import cn.edu.scnu.usercenter.model.domain.User;
import cn.edu.scnu.usercenter.model.vo.UserVO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author 崔恒拓
 * @version 1.0
 */
@SpringBootTest
@RunWith(SpringRunner.class)
class UserServiceTest {
    @Resource
    private UserService userService;
    @Test
    public void testAddUser(){
        User user = new User();
        user.setUsername("ccc");
        user.setUserAccount("dogC");
        user.setAvatarUrl("https://cn.bing.com/images/search?view=detailV2&ccid=vvmL0Z5r&id=0AC29CCE3629CAE65CA119E916B17B8799D5545D&thid=OIP.vvmL0Z5r6PhqUH0HGR-P6wAAAA&mediaurl=https%3a%2f%2fts1.cn.mm.bing.net%2fth%2fid%2fR-C.bef98bd19e6be8f86a507d07191f8feb%3frik%3dXVTVmYd7sRbpGQ%26riu%3dhttp%253a%252f%252ftupian.qqw21.com%252farticle%252fUploadPic%252f2019-7%252f201971622263525455.jpeg%26ehk%3dmzqsinDGdR%252ftn%252bHUhQXGUWIxhqNrlQvvv6I4xCD4la0%253d%26risl%3d%26pid%3dImgRaw%26r%3d0%26sres%3d1%26sresct%3d1%26srh%3d800%26srw%3d800&exph=400&expw=400&q=%e5%a4%b4%e5%83%8f&simid=608035677315688091&FORM=IRPRST&ck=ADD3CAC17FF6D71C9632C0E99366B35F&selectedIndex=4&ajaxhist=0&ajaxserp=0");
        user.setGender(0);
        user.setUserPassword("xxx");
        user.setPhone("123");
        user.setEmail("456");
        boolean result = userService.save(user);
        System.out.println(user.getId());
        Assertions.assertEquals(true, result);
    }

    @Test
    void userRegister() {
        String userAccount = "dogC";
        String userPassowrd = "";
        String checkPassowrd = "123456";
        String planetCode = "1";
        long result = userService.userRegister(userAccount, userPassowrd, checkPassowrd, planetCode);
        Assertions.assertEquals(-1, result);
        userAccount = "cc";
        result = userService.userRegister(userAccount, userPassowrd, checkPassowrd, planetCode);
        Assertions.assertEquals(-1, result);
        userAccount = "dogC";
        userPassowrd = "123456";
        result = userService.userRegister(userAccount, userPassowrd, checkPassowrd, planetCode);
        Assertions.assertEquals(-1, result);
        userAccount = "dog C";
        userPassowrd = "12345678";
        checkPassowrd = "12345678";
        result = userService.userRegister(userAccount, userPassowrd, checkPassowrd, planetCode);
        Assertions.assertEquals(-1, result);
        userAccount = "dogC";
        userPassowrd = "12345678";
        checkPassowrd = "123456789";
        result = userService.userRegister(userAccount, userPassowrd, checkPassowrd, planetCode);
        Assertions.assertEquals(-1, result);
        userAccount = "dogC";
        userPassowrd = "12345678";
        checkPassowrd = "12345678";
        result = userService.userRegister(userAccount, userPassowrd, checkPassowrd, planetCode);
        Assertions.assertEquals(-1, result);
        userAccount = "dogCht";
        result = userService.userRegister(userAccount, userPassowrd, checkPassowrd, planetCode);
        Assertions.assertTrue(result < 0);
    }

    @Test
    void searchUsersByTags() {
        List<String> list = Arrays.asList("java", "python");
        List<UserVO> users = userService.searchUsersByTagsByMemory(list);
        Assertions.assertNotNull(users);
        System.out.println(users.size());
    }
}