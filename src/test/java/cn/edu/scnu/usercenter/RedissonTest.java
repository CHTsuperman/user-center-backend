package cn.edu.scnu.usercenter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.Redisson;
import org.redisson.api.RList;
import org.redisson.api.RedissonClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author 崔恒拓
 * @version 1.0
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class RedissonTest {

    @Resource
    private RedissonClient redissonClient;

    @Test
    public void test(){
        RList<String> list = redissonClient.getList("test-list");
        list.add("cht");

        System.out.println(list.get(0));
    }
}
