package cn.edu.scnu.usercenter;

import cn.edu.scnu.usercenter.mapper.UserMapper;
import cn.edu.scnu.usercenter.model.domain.User;
import cn.edu.scnu.usercenter.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runner.Runner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StopWatch;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * @author 崔恒拓
 * @version 1.0
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class InsertUsersTest {
    @Resource
    private UserMapper userMapper;

    @Resource
    private UserService userService;

    /**
     * corePoolSize – 默认同时运行多少线程，也就是初始时线程池中的线程数
     * maximumPoolSize – 最大允许的线程数
     * keepAliveTime –  线程的存活时间
     * unit – 线程存活时间的时间单位
     * workQueue –  任务队列
     * 当任务队列满了，积压了太多的任务，这时候就会对线程池进行扩容，直到增加到设置的最大线程池线程数量。
     * 如果已经到达了最大线程池线程数量，但工作队列还是满的，那就可以取指定一下任务策略，
     * 默认如果还有新来的任务，直接拒绝并报 RejectedExecutionException 错误。我们可以自定义策略
     */
    private ExecutorService executorService = new ThreadPoolExecutor(60, 1000, 10000, TimeUnit.MINUTES, new ArrayBlockingQueue<>(10000));

    /**
     * 批量插入用户
     */
    @Test
    public void insertUsers(){
        // spring提供的倒计时工具类
        StopWatch stopWatch = new StopWatch();
        final int INSERT_NUM = 1000;
        stopWatch.start();
        List<User> userList = new ArrayList<>();
        for (int i = 0; i < INSERT_NUM; i++) {
            User user = new User();
            user.setUsername("fakeData");
            user.setUserAccount("fakeData");
            user.setAvatarUrl("https://tse3-mm.cn.bing.net/th/id/OIP-C.vvmL0Z5r6PhqUH0HGR-P6wAAAA?pid=ImgDet&rs=1");
            user.setGender(0);
            user.setUserPassword("12345678");
            user.setProfile("fake people");
            user.setPhone("123");
            user.setEmail("123@qq.com");
            user.setUserStatus(0);
            user.setUserRole(0);
            user.setPlanetCode("111111");
            user.setTags("[]");
            userList.add(user);
        }
        userService.saveBatch(userList, 100);
        stopWatch.stop();
        System.out.println(stopWatch.getTotalTimeMillis());
    }

    /**
     * 并发批量插入用户
     */
    @Test
    public void insertConcurrencyUsers(){
        // spring提供的倒计时工具类
        StopWatch stopWatch = new StopWatch();
        final int INSERT_NUM = 100000;
        stopWatch.start();
        // 分10组
        List<CompletableFuture<Void>> futureList = new ArrayList<>();
        int j = 0;
        int batchSize = 2500;
        for (int i = 0; i < 40; i++) {
            List<User> userList = new ArrayList<>();
            while(true){
                j++;
                User user = new User();
                user.setUsername("fakeData");
                user.setUserAccount("fakeData");
                user.setAvatarUrl("https://tse3-mm.cn.bing.net/th/id/OIP-C.vvmL0Z5r6PhqUH0HGR-P6wAAAA?pid=ImgDet&rs=1");
                user.setGender(0);
                user.setUserPassword("12345678");
                user.setProfile("fake people");
                user.setPhone("123");
                user.setEmail("123@qq.com");
                user.setUserStatus(0);
                user.setUserRole(0);
                user.setPlanetCode("111111");
                user.setTags("[]");
                userList.add(user);
                if(j % batchSize == 0){
                    break;
                }
            }
            // 这里面执行的方法就是异步的
            CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
                System.out.println("threadName: " + Thread.currentThread().getName());
                userService.saveBatch(userList, batchSize);
            }, executorService);
            futureList.add(future);
        }
        // 如果不用join，程序是异步的，对于倒计时来说，一开始程序就结束了，
        // 因此需要用join阻塞一下，直到所有的异步任务结束，才执行下一条语句
        CompletableFuture.allOf(futureList.toArray(new CompletableFuture[]{})).join();
        stopWatch.stop();
        System.out.println(stopWatch.getTotalTimeMillis());
    }
}
