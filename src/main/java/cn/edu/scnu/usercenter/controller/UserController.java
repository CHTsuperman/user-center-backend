package cn.edu.scnu.usercenter.controller;

import cn.edu.scnu.usercenter.common.BaseResponse;
import cn.edu.scnu.usercenter.common.ErrorCode;
import cn.edu.scnu.usercenter.common.ResultUtils;
import cn.edu.scnu.usercenter.exception.BusinessException;
import cn.edu.scnu.usercenter.model.domain.User;
import cn.edu.scnu.usercenter.model.request.UserLoginRequest;
import cn.edu.scnu.usercenter.model.request.UserRegisterRequest;
import cn.edu.scnu.usercenter.model.vo.UserVO;
import cn.edu.scnu.usercenter.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 崔恒拓
 * @version 1.0
 */
@RestController
@RequestMapping("/user")
@Slf4j
//@CrossOrigin(origins = "http://localhost:5173/", allowCredentials = "true")
public class UserController {
    @Resource
    private UserService userService;

    @Resource
    private RedisTemplate<String, Object> redisTemplate;
    
    @PostMapping("/register")
    public BaseResponse<Long> userRegister(@RequestBody UserRegisterRequest userRegisterRequest) {
        if (userRegisterRequest == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        String userAccount = userRegisterRequest.getUserAccount();
        String userPassword = userRegisterRequest.getUserPassword();
        String checkPassword = userRegisterRequest.getCheckPassword();
        String planetCode = userRegisterRequest.getPlanetCode();
        /*
            这里做校验的原因是(service层中也有)：
                1. controller更倾向于对请求参数本身的校验，不涉及业务逻辑本身(越少越好)
                2. service层是对业务逻辑的校验(有可能被controller之外的类调用，因此service中还是要做)
         */
        if (StringUtils.isAnyBlank(userAccount, userPassword, checkPassword, planetCode)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        long userId = userService.userRegister(userAccount, userPassword, checkPassword, planetCode);
        return ResultUtils.success(userId);
    }

    @PostMapping("/login")
    public BaseResponse<UserVO> userLogin(@RequestBody UserLoginRequest userLoginRequest, HttpServletRequest request) {
        if (userLoginRequest == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        String userAccount = userLoginRequest.getUserAccount();
        String userPassword = userLoginRequest.getUserPassword();
        /*
            这里做校验的原因是(service层中也有)：
                1. controller更倾向于对请求参数本身的校验，不涉及业务逻辑本身(越少越好)
                2. service层是对业务逻辑的校验(有可能被controller之外的类调用，因此service中还是要做)
         */
        if (StringUtils.isAnyBlank(userAccount, userPassword)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        UserVO user = userService.userLogin(userAccount, userPassword, request);
        return ResultUtils.success(user);
    }

    @PostMapping("/logout")
    public BaseResponse<Integer> userLogout(HttpServletRequest request) {
        if (request == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        int result = userService.userLogout(request);
        return ResultUtils.success(result);
    }

    @GetMapping("/current")
    public BaseResponse<UserVO> getCurrentUser(HttpServletRequest request) {
        UserVO currentUser = userService.getLoginUser(request);
        if (currentUser == null) {
            throw new BusinessException(ErrorCode.NOT_LOGIN, "未登录");
        }
        Long userId = currentUser.getId();
        // TODO 校验用户是否合法
        User user = userService.getById(userId);
        UserVO safetyUser = userService.getSafetyUser(user);
        return ResultUtils.success(safetyUser);
    }


    @GetMapping("/search")
    public BaseResponse<List<UserVO>> searchUsers(String username, HttpServletRequest request) {
        // 仅管理员可查询
        if (!userService.isAdmin(request)) {
            throw new BusinessException(ErrorCode.NOT_AUTH, "非管理员无权限");
        }
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(username)) {
            queryWrapper.like("username", username);
        }
        List<User> userList = userService.list(queryWrapper);
        List<UserVO> result = userList.stream().map(user -> userService.getSafetyUser(user)).collect(Collectors.toList());
        return ResultUtils.success(result);
    }

    @GetMapping("/search/tags")
    public BaseResponse<List<UserVO>> searchUserByTags(@RequestParam(required = false) List<String> tagNameList) {
        if (CollectionUtils.isEmpty(tagNameList)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "请求列表为空");
        }
        List<UserVO> userList = userService.searchUsersByTagsByMemory(tagNameList);
        return ResultUtils.success(userList);
    }

    @GetMapping("/recommend")
    public BaseResponse<Page<User>> recommendUsers(long pageSize, long pageNum, HttpServletRequest request) {
        if(request == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Page<User> userPage = userService.recommendUsersById(pageSize, pageNum, request);
        return ResultUtils.success(userPage);
    }

    @PostMapping("/update/self")
    public BaseResponse<Integer> updateUserBySelf(@RequestBody User user, HttpServletRequest request) {
        if (user == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        if(updateDataIsNull(user)){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        UserVO loginUser = userService.getLoginUser(request);
        int result = userService.updateUserBySelf(user, loginUser);
        return ResultUtils.success(result);
    }

    @PostMapping("/delete")
    public BaseResponse<Boolean> deleteUser(@RequestBody long id, HttpServletRequest request) {
        if (!userService.isAdmin(request)) {
            throw new BusinessException(ErrorCode.NOT_AUTH, "非管理员无权限");
        }
        if (id <= 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "id小于等于0");
        }
        boolean result = userService.removeById(id);
        return ResultUtils.success(result);
    }

    /**
     * 获取最匹配的用户
     * @param num
     * @param request
     * @return
     */
    @GetMapping("/match")
    public BaseResponse<List<UserVO>> matchUsers(long num, HttpServletRequest request){
        if(num <=0 || num >= 20){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        UserVO loginUser = userService.getLoginUser(request);
        return ResultUtils.success(userService.matchUser(num, loginUser));
    }


    private boolean updateDataIsNull(User user) {
        if (user == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        String username = user.getUsername();
        String avatarUrl = user.getAvatarUrl();
        Integer gender = user.getGender();
        String phone = user.getPhone();
        String email = user.getEmail();

        if(!StringUtils.isEmpty(username) && !StringUtils.isWhitespace(username)){
            return false;
        }
        if(!StringUtils.isEmpty(avatarUrl) && !StringUtils.isWhitespace(avatarUrl)){
            return false;
        }
        if(gender != null){
            return false;
        }
        if(!StringUtils.isEmpty(phone) && !StringUtils.isWhitespace(phone)){
            return false;
        }
        if(!StringUtils.isEmpty(email) && !StringUtils.isWhitespace(email)){
            return false;
        }
        return true;
    }


}
