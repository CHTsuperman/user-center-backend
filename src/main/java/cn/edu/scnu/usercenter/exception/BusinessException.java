package cn.edu.scnu.usercenter.exception;

import cn.edu.scnu.usercenter.common.ErrorCode;

/**
 * @author 崔恒拓
 * @version 1.0
 */
public class BusinessException extends RuntimeException{

    private final int statusCode;
    private final String description;

    public BusinessException(String message, int statusCode, String description) {
        super(message);
        this.statusCode = statusCode;
        this.description = description;
    }

    public BusinessException(ErrorCode errorCode) {
        super(errorCode.getMessage());
        this.statusCode = errorCode.getStatusCode();
        this.description = errorCode.getDescription();
    }

    public BusinessException(ErrorCode errorCode, String description) {
        super(errorCode.getMessage());
        this.statusCode = errorCode.getStatusCode();
        this.description = description;
    }


    public int getStatusCode() {
        return statusCode;
    }

    public String getDescription() {
        return description;
    }
}
