package cn.edu.scnu.usercenter.job;

import cn.edu.scnu.usercenter.config.RedisTemplateConfig;
import cn.edu.scnu.usercenter.mapper.UserMapper;
import cn.edu.scnu.usercenter.model.domain.User;
import cn.edu.scnu.usercenter.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

/**
 * 缓存预热任务
 * @author 崔恒拓
 * @version 1.0
 */

// TODO 如果为了多结合一些技术，后期可以整合以下Quartz任务框架
@Component
@Slf4j
public class PreCacheJob {
    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Resource
    private UserService userService;

    // 重点用户
    // TODO 可以不写死，动态配置，写到配置中心中，或者读取mysql中的数据(可以加一个字段，是否为重点用户)，或者读取redis中的用户
    private List<Long> mainUserList = Arrays.asList(7L);

    @Resource
    private RedissonClient redissonClient;

    /**
     * 每天执行，预热推荐用户
     */
    @Scheduled(cron = "0 3 21 * * *")
    public void  doCacheRecommend(){
        RLock lock = redissonClient.getLock("tami:precachejob:docache:lock");
        try {
            // 只有一个线程能获取到锁
            if(lock.tryLock(0, -1, TimeUnit.SECONDS)){
                System.out.println("get lock " + Thread.currentThread().getId());
                for(Long mainUserId:mainUserList) {
                    String key = String.format("tami:user:recommend:%s", mainUserId);
                    ValueOperations<String, Object> opsForValue = redisTemplate.opsForValue();
                    QueryWrapper<User> queryWrapper = new QueryWrapper<>();
                    Page<User> userPage = userService.page(new Page<>(1, 20), queryWrapper);
                    try {
                        opsForValue.set(key, userPage, 86400, TimeUnit.SECONDS);
                    } catch (Exception e) {
                        log.info("redis set key error", e);
                    }
                }
            }
        } catch (InterruptedException e) {
            log.error("cache recommend task error" + e);
        } finally {
            if(lock.isHeldByCurrentThread()){
                System.out.println("unlock " + Thread.currentThread().getId());
                lock.unlock();
            }
        }

    }

}
