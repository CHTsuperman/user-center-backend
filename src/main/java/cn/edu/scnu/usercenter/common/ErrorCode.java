package cn.edu.scnu.usercenter.common;

/**
 * @author 崔恒拓
 * @version 1.0
 */
public enum ErrorCode {
    SUCCESS(20000, "ok", ""),
    PARAMS_ERROR(40000, "请求参数错误", ""),
    NULL_ERROR(40001, "请求数据为空", ""),
    NOT_LOGIN(40100, "未登录", ""),
    NOT_AUTH(40101, "无权限", ""),
    FORBIDDEN(40301, "禁止操作", ""),
    SYSTEM_ERROR(50000, "系统内错误", "");

    private int statusCode;
    /**
     * 状态码信息
     */
    private String message;
    /**
     * 状态码信息（详情）,一般给前端使用
     */
    private String description;

    ErrorCode(int statusCode, String message, String description) {
        this.statusCode = statusCode;
        this.message = message;
        this.description = description;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

    public String getDescription() {
        return description;
    }
}
