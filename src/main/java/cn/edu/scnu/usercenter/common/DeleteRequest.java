package cn.edu.scnu.usercenter.common;

import lombok.Data;

import java.io.Serializable;

/**
 * 通用的删除请求参数
 * @author 崔恒拓
 * @version 1.0
 */
@Data
public class DeleteRequest implements Serializable {
    private static final long serialVersionUID = 765539972638063054L;

    private long id;
}
