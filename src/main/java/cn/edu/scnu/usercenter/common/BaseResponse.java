package cn.edu.scnu.usercenter.common;

import cn.edu.scnu.usercenter.exception.BusinessException;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 崔恒拓
 * @version 1.0
 */

/**
 * 通用返回类
 * @param <T>
 */
@Data
public class BaseResponse<T> implements Serializable {

    private static final long serialVersionUID = -595252693526606704L;

    private int code;

    private T data;

    private String message;

    private String description;

    public BaseResponse(int code, T data, String message, String description) {
        this.code = code;
        this.data = data;
        this.message = message;
        this.description = description;
    }

    public BaseResponse(int code, T data, String message) {
        this(code, data, message, "");
    }

    public BaseResponse(int code, T data) {
        this(code, data, "", "");
    }

    public BaseResponse(ErrorCode errorCode){
        this(errorCode.getStatusCode(), null, errorCode.getMessage(), errorCode.getDescription());
    }

    public BaseResponse(int code, String message, String description){
        this(code, null, message, description);
    }

}
