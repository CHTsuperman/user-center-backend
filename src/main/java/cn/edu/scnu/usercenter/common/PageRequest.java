package cn.edu.scnu.usercenter.common;

import lombok.Data;

import java.io.Serializable;

/**
 * 通用分页请求参数
 * @author 崔恒拓
 * @version 1.0
 */
@Data
public class PageRequest implements Serializable {
    private static final long serialVersionUID = 1174299690004062986L;

    /**
     * 查询条数
     */
    protected int pageSize = 10;

    /**
     * 页面大小
     */
    protected int pageNum = 1;
}
