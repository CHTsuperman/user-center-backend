package cn.edu.scnu.usercenter.common;

import cn.edu.scnu.usercenter.exception.BusinessException;

/**
 * @author 崔恒拓
 * @version 1.0
 */
public class ResultUtils {
    /**
     * 成功
     * @param data
     * @param <T>
     * @return
     */
    public static <T> BaseResponse<T> success(T data){
        return new BaseResponse<>(20000, data, "ok");
    }

    /**
     * 失败
     * @param errorCode
     * @return
     */
    public static BaseResponse error(ErrorCode errorCode){
        return new BaseResponse<>(errorCode);
    }

    /**
     * 失败
     * @param statusCode
     * @param message
     * @param description
     * @return
     */
    public static BaseResponse error(int statusCode, String message, String description){
        return new BaseResponse<>(statusCode, message, description);
    }

    /**
     * 失败
     * @param errorCode
     * @param message
     * @param description
     * @return
     */
    public static BaseResponse error(ErrorCode errorCode, String message, String description){
        return new BaseResponse<>(errorCode.getStatusCode(), message, description);
    }
}
