package cn.edu.scnu.usercenter.mapper;

import cn.edu.scnu.usercenter.model.domain.Team;
import cn.edu.scnu.usercenter.model.domain.User;
import cn.edu.scnu.usercenter.model.vo.UserVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
* @author 崔恒拓
* @description 针对表【team(队伍表)】的数据库操作Mapper
* @createDate 2022-10-20 14:09:36
* @Entity cn.edu.scnu.usercenter.model.domain.Team
*/
public interface TeamMapper extends BaseMapper<Team> {
    List<UserVO> searchUsersInTeam(Long teamId);
}




