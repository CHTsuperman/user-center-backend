package cn.edu.scnu.usercenter.mapper;

import cn.edu.scnu.usercenter.model.domain.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 崔恒拓
* @description 针对表【user(用户表)】的数据库操作Mapper
* @createDate 2022-10-03 21:16:40
* @Entity cn.edu.scnu.usercenter.model.domain.User
*/
public interface UserMapper extends BaseMapper<User> {

}




