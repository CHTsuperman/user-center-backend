package cn.edu.scnu.usercenter.mapper;

import cn.edu.scnu.usercenter.model.domain.UserTeam;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 崔恒拓
* @description 针对表【user_team(用户队伍关系表)】的数据库操作Mapper
* @createDate 2022-10-20 14:11:57
* @Entity cn.edu.scnu.usercenter.model.domain.UserTeam
*/
public interface UserTeamMapper extends BaseMapper<UserTeam> {

}




