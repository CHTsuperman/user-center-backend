package cn.edu.scnu.usercenter.service.impl;

import cn.edu.scnu.usercenter.common.ErrorCode;
import cn.edu.scnu.usercenter.common.ResultUtils;
import cn.edu.scnu.usercenter.exception.BusinessException;
import cn.edu.scnu.usercenter.model.domain.User;
import cn.edu.scnu.usercenter.model.request.TeamJoinRequest;
import cn.edu.scnu.usercenter.model.vo.UserVO;
import cn.edu.scnu.usercenter.utils.AlgorithmUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.edu.scnu.usercenter.service.UserService;
import cn.edu.scnu.usercenter.mapper.UserMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import javafx.util.Pair;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static cn.edu.scnu.usercenter.constant.UserConstant.ADMIN_ROLE;
import static cn.edu.scnu.usercenter.constant.UserConstant.USER_LOGIN_STATE;

/**
 * @author 崔恒拓
 * @description 针对表【user(用户表)】的数据库操作Service实现
 * @createDate 2022-10-03 21:16:40
 */
@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
        implements UserService {

    @Resource
    private UserMapper userMapper;

    /**
     * 盐值，混淆密码
     */
    private static final String SALT = "dogC";

    @Resource
    private RedisTemplate<String, Object> redisTemplate;


    @Override
    public long userRegister(String userAccount, String userPassword, String checkPassword, String planetCode) {
        // 校验是否为空
        if (StringUtils.isAnyBlank(userAccount, userPassword, checkPassword, planetCode)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "存在参数为空");
        }
        // 账号需大于等于4位，密码需大于等于8位
        if (userAccount.length() < 4) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账号长度小于4位");
        }
        if (userPassword.length() < 8 || checkPassword.length() < 8) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "密码小于8位");
        }
        if(planetCode.length() > 5){
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "编号过长");
        }
        // 账号不能包含特殊字符
        String validPattern = "^[a-zA-Z0-9_]+$";
        Matcher matcher = Pattern.compile(validPattern).matcher(userAccount);
        if (!matcher.matches()) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账号包含特殊字符");
        }
        // 密码和校验密码一致
        if (!userPassword.equals(checkPassword)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "两次密码不一致");
        }
        // 账号不能重复(放在后面，不浪费性能，因为其实前面的不通过的话，没必要进行这一步，查询数据库是相对费时费资源的操作)
        QueryWrapper<User> query = new QueryWrapper<>();
        query.eq("userAccount", userAccount);
        int count = this.count(query);
        if (count > 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账号已被注册");
        }
        // 编号不能重复
        query = new QueryWrapper<>();
        query.eq("planetCode", planetCode);
        count = this.count(query);
        if (count > 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "编号已被注册");
        }
        // 密码加密
        String encryptPassword = DigestUtils.md5DigestAsHex((SALT + userPassword).getBytes(StandardCharsets.UTF_8));
        // 插入数据
        User user = new User();
        user.setUserAccount(userAccount);
        user.setUserPassword(encryptPassword);
        user.setPlanetCode(planetCode);
        boolean saveResult = this.save(user);
        if (!saveResult) {
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "注册失败");
        }
        Long userId = user.getId();
        if(userId == null){
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "注入userId失败");
        }
        return userId;
    }

    @Override
    public UserVO userLogin(String userAccount, String userPassword, HttpServletRequest request) {
        // 校验是否为空
        if (StringUtils.isAnyBlank(userAccount, userPassword)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "存在参数为空");
        }
        // 账号需大于等于4位，密码需大于等于8位
        if (userAccount.length() < 4) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账号长度小于4位");
        }
        if (userPassword.length() < 8) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "密码小于8位");
        }
        // 账号不能包含特殊字符
        String validPattern = "^[a-zA-Z0-9_]+$";
        Matcher matcher = Pattern.compile(validPattern).matcher(userAccount);
        if (!matcher.matches()) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账号包含特殊字符");
        }
        // 密码加密
        String encryptPassword = DigestUtils.md5DigestAsHex((SALT + userPassword).getBytes(StandardCharsets.UTF_8));
        // 查询密码是否一致
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userAccount", userAccount);
        queryWrapper.eq("userPassword", encryptPassword);
        User user = userMapper.selectOne(queryWrapper);
        if (user == null) {
            log.info("user login failed. userAccount can not match userPassword");
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账号和密码不匹配");
        }
        // 用户脱敏
        UserVO safetyUser = getSafetyUser(user);
        // 记录用户的登陆态
        request.getSession().setAttribute(USER_LOGIN_STATE, safetyUser);
        return safetyUser;
    }

    @Override
    public UserVO getSafetyUser(User user) {
        if(user == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "当前对象为空");
        }
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(user, userVO);
        return userVO;
    }

    @Override
    public int userLogout(HttpServletRequest request) {
        // 移除登陆态
        request.getSession().removeAttribute(USER_LOGIN_STATE);
        return 1;
    }

    /**
     * 用内存查询(也可以和数据库查询结合)
     * @param tagNameList 用户要拥有的标签
     * @return
     */
    @Override
    public List<UserVO> searchUsersByTagsByMemory(List<String> tagNameList) {
        if(CollectionUtils.isEmpty(tagNameList)){
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "请求列表为空");
        }
        List<User> userList = this.list();
        Gson gson = new Gson();
        return userList.parallelStream().filter(user -> {
            String tagsStr = user.getTags();
            if(StringUtils.isBlank(tagsStr)){
                return false;
            }
            Set<String> tempTagNameSet = gson.fromJson(tagsStr, new TypeToken<Set<String>>(){}.getType());
            tempTagNameSet = Optional.ofNullable(tempTagNameSet).orElse(new HashSet<>());
            for(String tagName:tagNameList){
                if(!tempTagNameSet.contains(tagName)){
                    return false;
                }
            }
            return true;
        }).map(this::getSafetyUser).collect(Collectors.toList());
    }

    @Override
    public int updateUserByAdmin(User user, UserVO loginUser) {
        Long userId = user.getId();
        if(userId == null || userId <= 0){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        if(!isAdmin(loginUser)){
            throw new BusinessException(ErrorCode.NOT_AUTH, "非管理员无权限");
        }
        User oldUser = userMapper.selectById(userId);
        if(oldUser == null){
            throw new BusinessException(ErrorCode.NULL_ERROR, "数据库中无此id用户");
        }
        return userMapper.updateById(user);

    }

    @Override
    public int updateUserBySelf(User user, UserVO loginUser) {
        Long userId = user.getId();
        if(userId == null || userId <= 0){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        if(!userId.equals(loginUser.getId())){
            throw new BusinessException(ErrorCode.NOT_AUTH, "无法修改非自身用户信息");
        }
        User oldUser = userMapper.selectById(userId);
        if(oldUser == null){
            throw new BusinessException(ErrorCode.NULL_ERROR, "数据库中无此id用户");
        }
        TreeMap<Integer, String> timeToToken = new TreeMap<>();
        return userMapper.updateById(user);
    }

    @Override
    public Page<User> recommendUsersById(long pageSize, long pageNum, HttpServletRequest request) {
        UserVO loginUser = getLoginUser(request);
        String key = String.format("tami:user:recommend:%s", loginUser.getId());
        ValueOperations<String, Object> opsForValue = redisTemplate.opsForValue();
        Page<User> userPage = (Page<User>) opsForValue.get(key);
        if(userPage != null){
            return userPage;
        }
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        userPage = page(new Page<>(pageNum, pageSize), queryWrapper);
        try {
            opsForValue.set(key, userPage, 10000, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            log.info("redis set key error", e);
        }
        return userPage;
    }

    @Override
    public UserVO getLoginUser(HttpServletRequest request) {
        if(request == null){
            return null;
        }
        UserVO loginUser = (UserVO) request.getSession().getAttribute(USER_LOGIN_STATE);
        if(loginUser == null){
            throw new BusinessException(ErrorCode.NOT_LOGIN, "用户未登录");
        }
        return loginUser;
    }

    @Override
    public UserVO getLoginUserAllowNull(HttpServletRequest request) {
        if(request == null){
            return null;
        }
        return (UserVO) request.getSession().getAttribute(USER_LOGIN_STATE);
    }

    @Override
    public Boolean isAdmin(HttpServletRequest request) {
        UserVO user = getLoginUser(request);
        return user != null && user.getUserRole() == ADMIN_ROLE;
    }

    @Override
    public Boolean isAdmin(UserVO loginUser) {
        return loginUser != null && loginUser.getUserRole() == ADMIN_ROLE;
    }

    // TODO 速度慢，可以使用分库分表技术
    @Override
    public List<UserVO> matchUser(long num, UserVO loginUser) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id", "tags");
        queryWrapper.isNotNull("tags");
        List<User> userList = this.list(queryWrapper);
        String tags = loginUser.getTags();
        Gson gson = new Gson();
        List<String> tagList = gson.fromJson(tags, new TypeToken<List<String>>() {
        }.getType());

        // <Long, Long> => <下标, 距离>
        List<Pair<User, Long>> list = new ArrayList<>();
        for (int i = 0; i < userList.size(); i++) {
            User user = userList.get(i);
            String userTags = user.getTags();
            if(StringUtils.isBlank(userTags) || loginUser.getId().equals(user.getId())){
                continue;
            }
            List<String> userTagList = gson.fromJson(userTags, new TypeToken<List<String>>() {
            }.getType());
            long distance = AlgorithmUtils.minDistance(tagList, userTagList);
            list.add(new Pair<>(user, distance));
        }
        List<Long> topUserList = list.stream()
                .sorted((a, b) -> (int) (a.getValue() - b.getValue()))
                .limit(num)
                .map(pair -> pair.getKey().getId())
                .collect(Collectors.toList());

        // 数据库中查出来的数据顺序不一致
        Map<Long, UserVO> userVOMap = this.listByIds(topUserList)
                .stream()
                .map(this::getSafetyUser)
                .collect(Collectors.toMap(UserVO::getId, userVO -> userVO));

        List<UserVO> userVOList = new ArrayList<>();
        for (Long topUserId : topUserList) {
            userVOList.add(userVOMap.get(topUserId));
        }
        return userVOList;
    }

    /**
     * 直接用数据库查询
     * @param tagNameList 用户要拥有的标签
     * @return
     */
    @Override
    public List<UserVO> searchUsersByTagsBySQL(List<String> tagNameList) {
        if(CollectionUtils.isEmpty(tagNameList)){
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "请求列表为空");
        }
        QueryWrapper<User> query = new QueryWrapper<>();
        for(String tagName:tagNameList){
            query.like("tags", tagName);
        }
        List<User> userList = this.list(query);
        return userList.stream().map(this::getSafetyUser).collect(Collectors.toList());
    }



}




