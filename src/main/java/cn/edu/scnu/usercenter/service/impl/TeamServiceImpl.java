package cn.edu.scnu.usercenter.service.impl;
import java.util.Date;

import cn.edu.scnu.usercenter.common.ErrorCode;
import cn.edu.scnu.usercenter.enums.TeamStatusEnum;
import cn.edu.scnu.usercenter.exception.BusinessException;
import cn.edu.scnu.usercenter.model.domain.User;
import cn.edu.scnu.usercenter.model.domain.UserTeam;
import cn.edu.scnu.usercenter.model.dto.TeamQuery;
import cn.edu.scnu.usercenter.model.request.TeamExitRequest;
import cn.edu.scnu.usercenter.model.request.TeamJoinRequest;
import cn.edu.scnu.usercenter.model.request.TeamUpdateRequest;
import cn.edu.scnu.usercenter.model.vo.TeamUserVO;
import cn.edu.scnu.usercenter.model.vo.UserVO;
import cn.edu.scnu.usercenter.service.UserService;
import cn.edu.scnu.usercenter.service.UserTeamService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.edu.scnu.usercenter.model.domain.Team;
import cn.edu.scnu.usercenter.service.TeamService;
import cn.edu.scnu.usercenter.mapper.TeamMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.redisson.RedissonMultiLock;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static cn.edu.scnu.usercenter.constant.TeamConstant.TEAM_LOCK;
import static cn.edu.scnu.usercenter.constant.TeamConstant.USER_LOCK;

/**
* @author 崔恒拓
* @description 针对表【team(队伍表)】的数据库操作Service实现
* @createDate 2022-10-20 14:09:36
*/
@Service
public class TeamServiceImpl extends ServiceImpl<TeamMapper, Team>
    implements TeamService{

    @Resource
    private TeamMapper teamMapper;

    @Resource
    private UserTeamService userTeamService;

    @Resource
    private UserService userService;

    @Resource
    private RedissonClient redissonClient;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public long addTeam(Team team, UserVO loginUser) {
        if(team == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        if(loginUser == null){
            throw new BusinessException(ErrorCode.NOT_LOGIN, "未登录");
        }
        final long userId = loginUser.getId();
        int maxNum = Optional.ofNullable(team.getMaxNum()).orElse(0);
        if(maxNum < 1 || maxNum > 20){
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "队伍人数不满足要求");
        }
        String name = team.getName();
        if(StringUtils.isEmpty(name) || StringUtils.isWhitespace(name)){
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "名称不能为空或空格");
        }
        if(name.length() > 20){
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "名称过长");

        }
        String description = team.getDescription();
        if(StringUtils.isNotBlank(description) && description.length() > 512){
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "描述过长");
        }
        int teamStatus = Optional.ofNullable(team.getTeamStatus()).orElse(0);
        TeamStatusEnum statusEnum = TeamStatusEnum.getEnumByValue(teamStatus);
        if(statusEnum == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "队伍状态不满足要求");
        }
        String teamPassword = team.getTeamPassword();
        if(TeamStatusEnum.SECRET.equals(statusEnum)){
            if(StringUtils.isEmpty(teamPassword) || teamPassword.length() > 32){
                throw new BusinessException(ErrorCode.PARAMS_ERROR, "密码不符合规则");
            }
        }
        Date expireTime = team.getExpireTime();
        if(new Date().after(expireTime)){
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "过期时间错误");
        }

        // 一个用户最多创建5个表
        QueryWrapper<Team> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userId", userId);
        int hasTeamNum = teamMapper.selectCount(queryWrapper);;
        if(hasTeamNum >= 5){
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "一个用户最多创建5个表");
        }

        team.setId(null);
        team.setUserId(userId);
        boolean result = save(team);
        Long teamId = team.getId();
        if(!result || teamId == null){
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "创建队伍失败");
        }

        UserTeam userTeam = new UserTeam();
        userTeam.setUserId(userId);
        userTeam.setTeamId(teamId);
        userTeam.setJoinTime(new Date());
        result = userTeamService.save(userTeam);
        if(!result){
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "创建队伍失败");
        }
        return teamId;
    }

    @Override
    public List<TeamUserVO> listTeams(TeamQuery teamQuery, boolean isAdmin, boolean isFollowSelf, HttpServletRequest request) {
        QueryWrapper<Team> queryWrapper = new QueryWrapper<>();
        if(teamQuery != null){
            Long id = teamQuery.getId();
            if(id != null && id > 0){
                queryWrapper.eq("id", id);
            }
            List<Long> idList = teamQuery.getIdList();
            if(CollectionUtils.isNotEmpty(idList)){
                queryWrapper.in("id", idList);
            }
            String searchText = teamQuery.getSearchText();
            if(StringUtils.isNotBlank(searchText)){
                queryWrapper.and(qw -> qw.like("name", searchText).or().like("description", searchText));
            }
            String name = teamQuery.getName();
            if(StringUtils.isNotBlank(name)){
                queryWrapper.like("name", name);
            }
            String description = teamQuery.getDescription();
            if(StringUtils.isNotBlank(description)){
                queryWrapper.like("description", description);
            }
            Integer maxNum = teamQuery.getMaxNum();
            if(maxNum != null && maxNum > 0){
                queryWrapper.eq("maxNum", maxNum);
            }
            Long userId = teamQuery.getUserId();
            if(userId != null && userId > 0){
                queryWrapper.eq("userId", userId);
            }
            Integer teamStatus = teamQuery.getTeamStatus();
            TeamStatusEnum statusEnum = TeamStatusEnum.getEnumByValue(teamStatus);
            if(statusEnum == null){
                statusEnum = TeamStatusEnum.PUBLIC;
            }
            if(!isAdmin && statusEnum.equals(TeamStatusEnum.PRIVATE)){
                throw new BusinessException(ErrorCode.NOT_AUTH, "无权查看");
            }
            if(!isFollowSelf){
                queryWrapper.eq("teamStatus", statusEnum.getValue());
            }
        }
        // 不展示已过期的队伍
        queryWrapper.and(qw -> qw.isNull("expireTime").or().gt("expireTime", new Date()));


        List<Team> teamList = this.list(queryWrapper);
        if(CollectionUtils.isEmpty(teamList)){
            return Collections.emptyList();
        }
        List<TeamUserVO> teamUserVOList = new ArrayList<>();
        for (Team team : teamList) {
            Long teamId = team.getId();
            if(teamId == null || teamId <= 0){
                throw new BusinessException(ErrorCode.SYSTEM_ERROR, "数据库查询数据出错");
            }
            List<UserVO> userVOList = teamMapper.searchUsersInTeam(teamId);
            TeamUserVO teamUserVO = new TeamUserVO();
            BeanUtils.copyProperties(team, teamUserVO);
            if(userVOList == null){
                log.warn(String.format("warn: teamId=%d has no user", teamId));
                teamUserVO.setCurTeamNums(0);
            }else{
                teamUserVO.setCurTeamNums(userVOList.size());
            }

            teamUserVO.setUserList(userVOList);
            // 设置登录用户是否属于该队伍
            List<Long> teamNumberIds = userVOList.stream().map(UserVO::getId).collect(Collectors.toList());
            UserVO loginUser = userService.getLoginUserAllowNull(request);
            if(loginUser == null || !teamNumberIds.contains(loginUser.getId())){
                teamUserVO.setIsCurUserInTeam(0);
            }else{
                teamUserVO.setIsCurUserInTeam(1);
            }
            teamUserVOList.add(teamUserVO);
        }
        return teamUserVOList;
    }

    @Override
    public boolean updateTeam(TeamUpdateRequest teamUpdateRequest, UserVO loginUser) {
        if (teamUpdateRequest == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        final Long teamId = teamUpdateRequest.getId();
        Team team = getTeamById(teamId);
        Boolean isAdmin = userService.isAdmin(loginUser);
        if(!isAdmin && !loginUser.getId().equals(team.getUserId())){
            throw new BusinessException(ErrorCode.NOT_AUTH, "无权修改");
        }
        // 如果改为加密房间则必须设置密码
        TeamStatusEnum curTeamStatusEnum = TeamStatusEnum.getEnumByValue(team.getTeamStatus());
        TeamStatusEnum statusEnum = TeamStatusEnum.getEnumByValue(teamUpdateRequest.getTeamStatus());
        if(!curTeamStatusEnum.equals(statusEnum)){
            if(TeamStatusEnum.SECRET.equals(statusEnum)){
                String teamPassword = teamUpdateRequest.getTeamPassword();
                if(StringUtils.isEmpty(teamPassword) || teamPassword.length() > 32){
                    throw new BusinessException(ErrorCode.PARAMS_ERROR, "密码不符合规则");
                }
            }
        }
        // 如果maxNum需要修改
        Integer maxNum = teamUpdateRequest.getMaxNum();
        if(maxNum != null && maxNum < 1){
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "最大人数设置错误");
        }
        // 查询当前队伍人数
        if(maxNum != null){
            QueryWrapper<UserTeam> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("teamId", teamId);
            int curTeamNums = userTeamService.count(queryWrapper);
            // 大于maxNum值，则不修改
            if(curTeamNums > maxNum){
                throw new BusinessException(ErrorCode.PARAMS_ERROR, "最大人数设置错误");
            }
        }
        Team updateTeam = new Team();
        BeanUtils.copyProperties(teamUpdateRequest, updateTeam);
        return this.updateById(updateTeam);
    }

    @Override
    public boolean joinTeam(TeamJoinRequest teamJoinRequest, UserVO loginUser) {
        if(teamJoinRequest == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        final Long teamId = teamJoinRequest.getTeamId();
        Team team = getTeamById(teamId);
        Date expireTime = team.getExpireTime();
        if(expireTime != null && expireTime.before(new Date())){
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "房间已过期");
        }
        Integer teamStatus = team.getTeamStatus();
        TeamStatusEnum statusEnum = TeamStatusEnum.getEnumByValue(teamStatus);
        if(TeamStatusEnum.PRIVATE.equals(statusEnum)){
            throw new BusinessException(ErrorCode.NOT_AUTH, "禁止加入私有队伍");
        }
        String teamPassword = teamJoinRequest.getTeamPassword();
        if(TeamStatusEnum.SECRET.equals(statusEnum)){
            if(StringUtils.isEmpty(teamPassword) || !teamPassword.equals(team.getTeamPassword())){
                throw new BusinessException(ErrorCode.PARAMS_ERROR, "房间密码错误");
            }
        }

        final Long userId = loginUser.getId();
        RLock userLock = redissonClient.getLock(USER_LOCK + userId);
        RLock teamLock = redissonClient.getLock(TEAM_LOCK + teamId);
        RedissonMultiLock multiLock = new RedissonMultiLock(userLock, teamLock);
        try {
            if(multiLock.tryLock(10, -1, TimeUnit.SECONDS)){
                QueryWrapper<UserTeam> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("userId", userId);
                int hasJoinNum = userTeamService.count(queryWrapper);
                if(hasJoinNum >= 5){
                    throw new BusinessException(ErrorCode.PARAMS_ERROR, "最多加入5个队伍" );
                }
                queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("userId", userId);
                queryWrapper.eq("teamId", teamId);
                int isJoin = userTeamService.count(queryWrapper);
                if(isJoin > 0){
                    throw new BusinessException(ErrorCode.PARAMS_ERROR, "不能重复加入");
                }
                long curTeamNums = countTeamUserByTeamId(teamId);
                if(curTeamNums >= team.getMaxNum()){
                    throw new BusinessException(ErrorCode.PARAMS_ERROR, "人数已满");
                }

                UserTeam userTeam = new UserTeam();
                userTeam.setUserId(userId);
                userTeam.setTeamId(teamId);
                userTeam.setJoinTime(new Date());
                return userTeamService.save(userTeam);
            }
        } catch (InterruptedException e) {
            log.error("join team error" + e);
        } finally {
            multiLock.unlock();
        }

        throw new BusinessException(ErrorCode.FORBIDDEN, "无效点击");
    }

    // TODO 加了锁吞吐量很差，如何优化（是不是锁的力度太大）
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean exitTeam(TeamExitRequest teamExitRequest, UserVO loginUser) {
        if(teamExitRequest == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        final Long teamId = teamExitRequest.getTeamId();
        Team team = getTeamById(teamId);
        final Long useId = loginUser.getId();
        QueryWrapper<UserTeam> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("teamId", teamId);
        queryWrapper.eq("userId", useId);
        int isJoin = userTeamService.count(queryWrapper);
        if(isJoin <= 0){
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "用户未加入此房");
        }

        long curTeamNums = countTeamUserByTeamId(teamId);
        // 队伍只剩1人
        if(curTeamNums == 1){
            this.removeById(teamId);
        }else{
            // 队伍还有其他人
            // 是否为队长
            // TODO 在team表中添加是否为队长字段，区分创建人和队长，目前默认创建人为队长
            if(team.getUserId().equals(useId)){
                // 把队伍转移给最早加入的用户
                queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("teamId", teamId);
                queryWrapper.last("order by id asc limit 2");
                List<UserTeam> userTeamList = userTeamService.list(queryWrapper);
                if(CollectionUtils.isEmpty(userTeamList) || userTeamList.size() < 2){
                    throw new BusinessException(ErrorCode.SYSTEM_ERROR);
                }
                UserTeam nextTeamLeader = userTeamList.get(1);
                Long nextTeamLeaderUserId = nextTeamLeader.getUserId();
                Team updateTeam = new Team();
                updateTeam.setId(teamId);
                updateTeam.setUserId(nextTeamLeaderUserId);
                boolean result = this.updateById(updateTeam);
                if(!result){
                    throw new BusinessException(ErrorCode.SYSTEM_ERROR, "更新队伍队长失败");
                }
            }
        }

        queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("teamId", teamId);
        queryWrapper.eq("userId", useId);
        return userTeamService.remove(queryWrapper);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteTeam(Long teamId, UserVO loginUser) {
        Team team = this.getById(teamId);
        Long userId = loginUser.getId();
        if(!team.getUserId().equals(userId)){
            throw new BusinessException(ErrorCode.NOT_AUTH, "不是队长无权解散队伍");
        }
        QueryWrapper<UserTeam> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("teamId", teamId);
        boolean result = userTeamService.remove(queryWrapper);
        if(!result){
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "移除关系表失败");
        }
        return this.removeById(teamId);
    }

    /**
     * 根据teamId查找队伍
     * @param teamId
     * @return
     */
    private Team getTeamById(Long teamId) {
        if(teamId == null || teamId <= 0){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Team team = this.getById(teamId);
        if(team == null){
            throw new BusinessException(ErrorCode.NULL_ERROR, "队伍不存在");
        }
        return team;
    }

    /**
     * 获取某队伍当前人数
     * @param teamId
     * @return
     */
    private long countTeamUserByTeamId(long teamId){
        QueryWrapper<UserTeam>  queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("teamId", teamId);
        return userTeamService.count(queryWrapper);
    }
}




