package cn.edu.scnu.usercenter.service;

import cn.edu.scnu.usercenter.model.domain.UserTeam;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 崔恒拓
* @description 针对表【user_team(用户队伍关系表)】的数据库操作Service
* @createDate 2022-10-20 14:11:57
*/
public interface UserTeamService extends IService<UserTeam> {

}
