package cn.edu.scnu.usercenter.service;

import cn.edu.scnu.usercenter.model.domain.User;
import cn.edu.scnu.usercenter.model.request.TeamJoinRequest;
import cn.edu.scnu.usercenter.model.vo.UserVO;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static cn.edu.scnu.usercenter.constant.UserConstant.ADMIN_ROLE;
import static cn.edu.scnu.usercenter.constant.UserConstant.USER_LOGIN_STATE;

/**
* @author 崔恒拓
* @description 针对表【user(用户表)】的数据库操作Service
* @createDate 2022-10-03 21:16:40
*/
public interface UserService extends IService<User> {

    /**
     * 用户注册
     * @param userAccount 用户账户
     * @param userPassword 用户密码
     * @param checkPassword 校验密码
     * @param planetCode 编号(唯一)
     * @return 新用户id
     */
    long userRegister(String userAccount, String userPassword, String checkPassword, String planetCode);

    /**
     * 用户登陆
     * @param userAccount 用户账户
     * @param userPassword 用户密码
     * @param request 请求信息
     * @return 脱敏后的用户信息
     */

    UserVO userLogin(String userAccount, String userPassword, HttpServletRequest request);


    /**
     * 用户脱敏
     * @param user 脱敏前用户
     * @return 脱敏后用户信息
     */
    UserVO getSafetyUser(User user);

    /**
     * 用户注销
     * @param request 请求
     * @return 是否注销成功
     */
    int userLogout(HttpServletRequest request);


    /**
     * 根据标签搜索用户(用户必须包含所有查询的标签)(SQL查询版)
     *
     * @param tagNameList 用户要拥有的标签
     * @return
     */
    @Deprecated
    List<UserVO> searchUsersByTagsBySQL(List<String> tagNameList);

    /**
     * 根据标签搜索用户(用户必须包含所有查询的标签)(内存查询版)
     * @param tagNameList
     * @return
     */
    List<UserVO> searchUsersByTagsByMemory(List<String> tagNameList);

    /**
     * 管理员更新用户信息
     * @param user
     * @param loginUser
     * @return
     */
    int updateUserByAdmin(User user, UserVO loginUser);

    /**
     * 用户更新自己的信息
     * @param user
     * @param loginUser
     * @return
     */
    int updateUserBySelf(User user, UserVO loginUser);

    /**
     * 根据不同用户推荐不同用户
     * @param pageSize
     * @param pageNum
     * @param request
     * @return
     */
    Page<User> recommendUsersById(long pageSize, long pageNum, HttpServletRequest request);

    /**
     * 获取当前登陆用户信息(不允许为null)
     * @return
     */
    UserVO getLoginUser(HttpServletRequest request);

    /**
     * 获取当前登陆用户信息(允许为null)
     * @return
     */
    UserVO getLoginUserAllowNull(HttpServletRequest request);

    /**
     * 是否为管理员
     * @param request HttpServletRequest
     * @return 是否为管理员
     */
    Boolean isAdmin(HttpServletRequest request);

    /**
     * 是否为管理员
     * @param loginUser User
     * @return 是否为管理员
     */
    Boolean isAdmin(UserVO loginUser);

    /**
     * 匹配用户
     * @param num
     * @param loginUser
     * @return
     */
    List<UserVO> matchUser(long num, UserVO loginUser);
}
