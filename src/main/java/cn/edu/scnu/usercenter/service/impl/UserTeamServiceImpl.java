package cn.edu.scnu.usercenter.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.edu.scnu.usercenter.model.domain.UserTeam;
import cn.edu.scnu.usercenter.service.UserTeamService;
import cn.edu.scnu.usercenter.mapper.UserTeamMapper;
import org.springframework.stereotype.Service;

/**
* @author 崔恒拓
* @description 针对表【user_team(用户队伍关系表)】的数据库操作Service实现
* @createDate 2022-10-20 14:11:57
*/
@Service
public class UserTeamServiceImpl extends ServiceImpl<UserTeamMapper, UserTeam>
    implements UserTeamService{

}




