package cn.edu.scnu.usercenter.service;

import cn.edu.scnu.usercenter.model.domain.Team;
import cn.edu.scnu.usercenter.model.domain.User;
import cn.edu.scnu.usercenter.model.dto.TeamQuery;
import cn.edu.scnu.usercenter.model.request.TeamExitRequest;
import cn.edu.scnu.usercenter.model.request.TeamJoinRequest;
import cn.edu.scnu.usercenter.model.request.TeamUpdateRequest;
import cn.edu.scnu.usercenter.model.vo.TeamUserVO;
import cn.edu.scnu.usercenter.model.vo.UserVO;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* @author 崔恒拓
* @description 针对表【team(队伍表)】的数据库操作Service
* @createDate 2022-10-20 14:09:36
*/
public interface TeamService extends IService<Team> {

    /**
     * 创建用户
     * @param team
     * @param loginUser
     * @return
     */
    long addTeam(Team team, UserVO loginUser);

    /**
     * 搜素队伍
     * @param teamQuery
     * @param isAdmin
     * @param isFollowSelf 是否查看自身队伍信息
     * @return
     */
    List<TeamUserVO> listTeams(TeamQuery teamQuery, boolean isAdmin, boolean isFollowSelf, HttpServletRequest request);

    /**
     * 修改队伍信息
     * @param teamUpdateRequest
     * @param loginUser
     * @return
     */
    boolean updateTeam(TeamUpdateRequest teamUpdateRequest, UserVO loginUser);

    /**
     * 加入队伍
     * @param teamJoinRequest
     * @param loginUser
     * @return
     */
    boolean joinTeam(TeamJoinRequest teamJoinRequest, UserVO loginUser);

    /**
     * 用户退出队伍
     * @param teamExitRequest
     * @param loginUser
     * @return
     */
    boolean exitTeam(TeamExitRequest teamExitRequest, UserVO loginUser);

    /**
     * 用户解散队伍
     * @param teamId
     * @param loginUser
     * @return
     */
    boolean deleteTeam(Long teamId, UserVO loginUser);
}
