package cn.edu.scnu.usercenter.config;

import lombok.Data;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;

/**
 * Redisson 配置
 *
 * @author 崔恒拓
 * @version 1.0
 */

@Configuration
@ConfigurationProperties(prefix = "spring.redis")
@Data
public class RedissonConfig {
    private String port;
    private String password;
    private String host;
    private int redissonDatabase;

    @Bean
    public RedissonClient redissonClient() {
        // 1. 创建配置
        Config config = new Config();
        config.useSingleServer()
                .setAddress(String.format("redis://%s:%s", host, port))
                .setDatabase(redissonDatabase)
                .setPassword(password);
        // 2. 创建 Redisson 实例
        RedissonClient redisson = Redisson.create(config);
        return redisson;
    }
}
