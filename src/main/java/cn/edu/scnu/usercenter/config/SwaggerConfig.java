package cn.edu.scnu.usercenter.config;

/**
 * @author 崔恒拓
 * @version 1.0
 */

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 * 自动以 Swagger 接口文档的配置
 */
@Configuration
@EnableSwagger2WebMvc
@Profile({"dev"})
public class SwaggerConfig {

    @Bean(value = "dockerBean")
    public Docket dockerBean() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                //扫描路径，获取controller层的接口
                .apis(RequestHandlerSelectors.basePackage("cn.edu.scnu.usercenter.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    /**
     * 这是构造器模式，用于生成api信息
     * @return
     */
    public ApiInfo apiInfo(){
        return new ApiInfoBuilder()
                //标题
                .title("恒拓用户中心")
                //简介
                .description("恒拓用户中心接口文档")
                .termsOfServiceUrl("https://gitlab.com/CHTsuperman/")
                //作者、网址http:localhost:8088/doc.html(这里注意端口号要与项目一致，如果你的端口号后面还加了前缀，就需要把前缀加上)、邮箱
                .contact(new Contact("cht","https://gitlab.com/CHTsuperman/","1163898467@qq.com"))
                //版本
                .version("1.0")
                .build();
    }
}