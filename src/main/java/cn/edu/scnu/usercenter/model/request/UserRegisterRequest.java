package cn.edu.scnu.usercenter.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户注册请求体
 * @author 崔恒拓
 * @version 1.0
 */
@Data
public class UserRegisterRequest implements Serializable {

    private static final long serialVersionUID = -6350218237422176794L;

    private String userAccount;
    private String userPassword;
    private String checkPassword;
    protected String planetCode;
}
