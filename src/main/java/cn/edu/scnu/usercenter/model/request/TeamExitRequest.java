package cn.edu.scnu.usercenter.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户退出队伍
 * @author 崔恒拓
 * @version 1.0
 */
@Data
public class TeamExitRequest implements Serializable {
    private static final long serialVersionUID = 855761537060330505L;
    /**
     * teamId
     */
    private Long teamId;

}
