package cn.edu.scnu.usercenter.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户注册请求体
 * @author 崔恒拓
 * @version 1.0
 */
@Data
public class UserLoginRequest implements Serializable {

    private static final long serialVersionUID = -8367039600868472169L;
    private String userAccount;
    private String userPassword;
}
