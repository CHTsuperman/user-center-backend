package cn.edu.scnu.usercenter.model.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 崔恒拓
 * @version 1.0
 */
@Data
public class TeamUpdateRequest implements Serializable {

    private static final long serialVersionUID = -1309202514785234389L;

    /**
     * id
     */
    private Long id;

    /**
     * 队伍名称
     */
    private String name;

    /**
     * 队伍描述
     */
    private String description;

    /**
     * 最大人数
     */
    private Integer maxNum;

    /**
     * 过期时间
     */
    private Date expireTime;

    /**
     * 队伍状态 0 - 公开 1 - 私有 2 - 加密
     */
    private Integer teamStatus;

    /**
     * 队伍密码
     */
    private String teamPassword;
}
