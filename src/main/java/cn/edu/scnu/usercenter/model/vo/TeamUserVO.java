package cn.edu.scnu.usercenter.model.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 队伍和用户信息封装类(脱敏)
 * @author 崔恒拓
 * @version 1.0
 */
@Data
public class TeamUserVO implements Serializable {
    private static final long serialVersionUID = 5411133554644766790L;
    /**
     * id
     */
    private Long id;

    /**
     * 队伍名称
     */
    private String name;

    /**
     * 队伍描述
     */
    private String description;

    /**
     * 最大人数
     */
    private Integer maxNum;

    /**
     * 过期时间
     */
    private Date expireTime;

    /**
     * 创建人id
     */
    private Long userId;

    /**
     * 队伍状态 0 - 公开 1 - 私有 2 - 加密
     */
    private Integer teamStatus;

    /**
     * 当前房间人数
     */
    private Integer curTeamNums;


    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 是否为登录用户加入的队伍 0 - 不是 1 - 是
     */
    private Integer isCurUserInTeam;

    /**
     * 入队用户列表
     */
    List<UserVO> userList;
}
