package cn.edu.scnu.usercenter.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 崔恒拓
 * @version 1.0
 */
@Data
public class TeamJoinRequest implements Serializable {

    private static final long serialVersionUID = 8828416740041055810L;
    /**
     * teamId
     */
    private Long teamId;

    /**
     * 队伍密码
     */
    private String teamPassword;
}
