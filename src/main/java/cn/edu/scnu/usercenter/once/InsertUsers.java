package cn.edu.scnu.usercenter.once;
import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import cn.edu.scnu.usercenter.mapper.UserMapper;
import cn.edu.scnu.usercenter.model.domain.User;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import javax.annotation.Resource;

/**
 * @author 崔恒拓
 * @version 1.0
 */

//@Component
public class InsertUsers {
    @Resource
    private UserMapper userMapper;


    /**
     * 批量插入用户
     */
    //@Scheduled(fixedDelay = Long.MAX_VALUE)
    public void doInsertUser(){
        // spring提供的倒计时工具类
        StopWatch stopWatch = new StopWatch();
        final int INSERT_NUM = 1000;
        stopWatch.start();
        for (int i = 0; i < INSERT_NUM; i++) {
            User user = new User();
            user.setUsername("fakeData");
            user.setUserAccount("fakeData");
            user.setAvatarUrl("https://tse3-mm.cn.bing.net/th/id/OIP-C.vvmL0Z5r6PhqUH0HGR-P6wAAAA?pid=ImgDet&rs=1");
            user.setGender(0);
            user.setUserPassword("12345678");
            user.setProfile("fake people");
            user.setPhone("123");
            user.setEmail("123@qq.com");
            user.setUserStatus(0);
            user.setUserRole(0);
            user.setPlanetCode("111111");
            user.setTags("[]");
            userMapper.insert(user);
        }
        stopWatch.stop();
        System.out.println(stopWatch.getTotalTimeMillis());
    }
}
