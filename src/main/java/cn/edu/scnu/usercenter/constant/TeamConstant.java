package cn.edu.scnu.usercenter.constant;

/**
 * @author 崔恒拓
 * @version 1.0
 */
public interface TeamConstant {
    /**
     * 同一时间只有一个用户可以操作房间Id号为teamId的房间
     */
    String TEAM_LOCK = "tami:team:joinTeam:teamId:";

    /**
     * Id为userId的用户在同一时间只能执行一次加入队伍的操作
     */
    String USER_LOCK = "tami:team:joinTeam:userId:";
}
