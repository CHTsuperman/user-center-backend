package cn.edu.scnu.usercenter.constant;

/**
 * 用户常量
 * @author 崔恒拓
 * @version 1.0
 */
public interface UserConstant {
    /**
     * 用户登陆态键
     */
    String USER_LOGIN_STATE = "userLoginState";

    /**
     * 默认权限
     */
    int DEFAULT_ROLE = 0;

    /**
     * 管理员权限
     */
    int ADMIN_ROLE = 1;
}
